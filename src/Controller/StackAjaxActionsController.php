<?php

namespace Drupal\stack_overflow\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\stack_overflow\Entity\StackOverflowEntityInterface;
use Drupal\stack_overflow\StackOverflowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * StackAjaxActionsController.
 */
class StackAjaxActionsController extends ControllerBase {

  /**
   * AjaxResponse.
   *
   * @var \Drupal\Core\Ajax\AjaxResponse
   */
  protected $ajaxResponse;

  /**
   * StackOverflowManager.
   *
   * @var \Drupal\stack_overflow\StackOverflowManager
   */
  protected $stackOverflowManager;

  /**
   * StackAjaxActionsController constructor.
   *
   * @param \Drupal\stack_overflow\StackOverflowManager $stack_overflow_manager
   *   StackOverflowManager.
   */
  public function __construct(StackOverflowManager $stack_overflow_manager) {
    $this->stackOverflowManager = $stack_overflow_manager;
    $this->ajaxResponse = new AjaxResponse();
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stack_overflow.manager')
    );
  }

  /**
   * Implementation Edit link.
   *
   * @param \Drupal\stack_overflow\Entity\StackOverflowEntityInterface $stack_overflow_entity
   *   StackOverflowEntityInterface.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function editAction(StackOverflowEntityInterface $stack_overflow_entity) {
    $form = $this->entityFormBuilder()->getForm($stack_overflow_entity, 'edit_ajax');

    return $this->ajaxResponse->addCommand(new HtmlCommand('#' . $stack_overflow_entity->getEntityTypeId() . '-' . $stack_overflow_entity->id(), $form));
  }

  /**
   * Implementation Delete link.
   *
   * @param \Drupal\stack_overflow\Entity\StackOverflowEntityInterface $stack_overflow_entity
   *   StackOverflowEntityInterface.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteAction(StackOverflowEntityInterface $stack_overflow_entity) {
    switch ($stack_overflow_entity->getEntityTypeId()) {
      case 'stack_comment':
        /** @var \Drupal\stack_overflow\Entity\StackOverflowEntityInterface $entity */
        $entity = $stack_overflow_entity->getRelatedEntity();
        $stack_overflow_entity->delete();

        // Render comments.
        $selector_details = '#details--' . $entity->getEntityTypeId() . '--' . $entity->id();
        $selector = $selector_details . ' .comment-list';
        $content = $this->stackOverflowManager->getRenderComments($entity) ?: '';
        $count_comment = $this->t('Comments: @count', [
          '@count' => $this->stackOverflowManager->getCountComments($entity),
        ]);

        $this->ajaxResponse->addCommand(new HtmlCommand($selector, $content));
        $this->ajaxResponse->addCommand(new HtmlCommand($selector_details . ' summary', $count_comment));
        break;

      case 'answer':
        $question = $stack_overflow_entity->get('question')->entity;
        $author_id = $stack_overflow_entity->getOwnerId();
        $stack_overflow_entity->delete();

        // Render answer entities.
        $content = $this->stackOverflowManager->getRenderAnswers($question) ?: '';
        $this->ajaxResponse->addCommand(new HtmlCommand('#answer-list', $content));

        // If user deleted own answer, render Add form answer.
        if ($this->currentUser()->id() == $author_id) {
          $this->ajaxResponse->addCommand(new AppendCommand('#answers', $this->stackOverflowManager->getAnswerForm($question)));
        }
        break;
    }

    // @todo Change count comment on the top OR rebuild all comments.
    return $this->ajaxResponse;
  }

}
