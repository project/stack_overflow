<?php

namespace Drupal\stack_overflow\Controller;

use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class AnswerEntityController.
 *
 *  Returns responses for Answer routes.
 */
class QuestionEntityController extends EntityViewController {

  /**
   * {@inheritDoc}
   */
  public function view(EntityInterface $question, $view_mode = 'full') {
    return parent::view($question, $view_mode);
  }

}
