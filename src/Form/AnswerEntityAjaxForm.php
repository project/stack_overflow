<?php

namespace Drupal\stack_overflow\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Implement "edit_ajax" form for Answer entity.
 *
 * @see \Drupal\stack_overflow\Entity\AnswerEntity
 */
class AnswerEntityAjaxForm extends AnswerEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['revision_log_message']['#access'] = FALSE;

    if (!is_null($form_state->get('is_new'))) {
      return $form;
    }

    $form['actions']['submit']['#ajax'] = [
      'callback' => '::ajaxEditCallback',
      'event' => 'click',
    ];

    $form['actions']['delete']['#access'] = FALSE;
    $form['actions']['submit']['#value'] = $this->t('Update');

    return $form;
  }

}
