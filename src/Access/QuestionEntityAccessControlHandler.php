<?php

namespace Drupal\stack_overflow\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Blank entity.
 *
 * @see \Drupal\stack_overflow\Entity\QuestionEntity.
 */
class QuestionEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\stack_overflow\Entity\QuestionEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished question entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published question entities');

      case 'update':
        if ($account->hasPermission('edit any question entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('edit own question entities'));

      case 'delete':
        if ($account->hasPermission('delete any question entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('delete own question entities'));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add question entities');
  }

}
