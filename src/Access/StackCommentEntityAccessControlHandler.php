<?php

namespace Drupal\stack_overflow\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Stack comment entity.
 *
 * @see \Drupal\stack_overflow\Entity\StackCommentEntity.
 */
class StackCommentEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\stack_overflow\Entity\StackCommentEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished stack comment entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published stack comment entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit stack comment entities');

      case 'delete':
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIfHasPermission($account, 'delete stack comment entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add stack comment entities');
  }

}
