<?php

namespace Drupal\stack_overflow\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\stack_overflow\StackOverflowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Answer entity.
 *
 * @see \Drupal\stack_overflow\Entity\AnswerEntity.
 */
class AnswerEntityAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * StackOverflowManager.
   *
   * @var \Drupal\stack_overflow\StackOverflowManager
   */
  protected $stackManager;

  /**
   * CurrentRouteMatch.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * AnswerEntityAccessControlHandler constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   EntityTypeInterface.
   * @param \Drupal\stack_overflow\StackOverflowManager $stack_overflow_manager
   *   StackOverflowManager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   CurrentRouteMatch.
   */
  public function __construct(EntityTypeInterface $entity_type, StackOverflowManager $stack_overflow_manager, CurrentRouteMatch $route_match) {
    parent::__construct($entity_type);

    $this->stackManager = $stack_overflow_manager;
    $this->currentRouteMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('stack_overflow.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\stack_overflow\Entity\AnswerEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished answer entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published answer entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit answer entities');

      case 'delete':
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIfHasPermission($account, 'delete answer entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $question = $this->currentRouteMatch->getParameters()->get('question');

    // If current request is AJAX get Question entity from another parameter.
    if (!$question) {
      $question = $this->currentRouteMatch->getParameters()->get('stack_overflow_entity')->get('question')->entity;
    }

    $is_answered = $this->stackManager->isUserAlreadyAddAnswer($account, $question);

    return AccessResult::allowedIf(!$is_answered)
      ->andIf(AccessResult::allowedIfHasPermission($account, 'add answer entities'));
  }

}
