<?php

namespace Drupal\stack_overflow\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Answer entities.
 *
 * @ingroup stack_overflow
 */
interface AnswerEntityInterface extends StackOverflowEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {}
