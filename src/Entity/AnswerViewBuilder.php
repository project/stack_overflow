<?php

namespace Drupal\stack_overflow\Entity;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Url;
use Drupal\stack_overflow\StackOverflowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * AnswerViewBuilder.
 */
class AnswerViewBuilder extends EntityViewBuilder {

  /**
   * StackOverflowManager.
   *
   * @var \Drupal\stack_overflow\StackOverflowManager
   */
  protected $stackOverflowManager;

  /**
   * AnswerViewBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   EntityTypeInterface.
   * @param \Drupal\stack_overflow\StackOverflowManager $stack_overflow_manager
   *   StackOverflowManager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   EntityRepositoryInterface.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   LanguageManagerInterface.
   * @param \Drupal\Core\Theme\Registry|null $theme_registry
   *   Registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface|null $entity_display_repository
   *   EntityDisplayRepositoryInterface.
   */
  public function __construct(EntityTypeInterface $entity_type, StackOverflowManager $stack_overflow_manager, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry = NULL, EntityDisplayRepositoryInterface $entity_display_repository = NULL) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);

    $this->stackOverflowManager = $stack_overflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('stack_overflow.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    parent::buildComponents($build, $entities, $displays, $view_mode);

    /** @var \Drupal\stack_overflow\Entity\StackCommentEntityInterface[] $entities */
    if (empty($entities)) {
      return;
    }

    foreach ($entities as $id => $entity) {
      $bundle = $entity->bundle();
      $display = $displays[$bundle];

      if ($display->getComponent('links')) {
        $build[$id]['links'] = [
          '#prefix' => '<ul class="links inline">',
          '#suffix' => '</ul>',
        ];

        $build[$id]['links']['edit'] = [
          '#type' => 'link',
          '#title' => $this->t('Edit'),
          '#attributes' => [
            'class' => 'use-ajax',
          ],
          '#url' => Url::fromRoute('stack_overflow.action.edit_content', [
            'entity_type' => $entity->getEntityTypeId(),
            'stack_overflow_entity' => $entity->id(),
          ]),
          '#access' => $entity->access('update'),
          '#prefix' => '<li class="so-answer-edit">',
          '#suffix' => '</li>',
        ];

        $build[$id]['links']['delete'] = [
          '#type' => 'link',
          '#title' => $this->t('Delete'),
          '#attributes' => [
            'class' => 'use-ajax',
          ],
          '#url' => Url::fromRoute('stack_overflow.action.delete_content', [
            'entity_type' => $entity->getEntityTypeId(),
            'stack_overflow_entity' => $entity->id(),
          ]),
          '#access' => $entity->access('delete'),
          '#prefix' => '<li class="so-answer-delete">',
          '#suffix' => '</li>',
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildMultiple(array $build_list) {
    $build_list = parent::buildMultiple($build_list);

    $children = Element::children($build_list);
    foreach ($children as $key) {
      $answer = $build_list[$key]['#answer'];

      $build_list[$key]['comments'] = [
        '#type' => 'details',
        '#title' => $this->t('Comments: @count', [
          '@count' => $this->stackOverflowManager->getCountComments($answer),
         ]),
        '#open' => FALSE,
        '#attributes' => [
          'id' => 'details--' . $answer->getEntityTypeId() . '--' . $answer->id(),
        ],
        '#weight' => 100,
      ];

      $build_list[$key]['comments']['list'] = [
        '#prefix' => '<div class="comment-list">',
        '#suffix' => '</div>',
      ];

      $build_list[$key]['comments']['list']['content'] = $this->stackOverflowManager->getRenderComments($answer);
      $build_list[$key]['comments']['form'] = $this->stackOverflowManager->getCommentForm($answer);
    }

    return $build_list;
  }

}
