<?php

namespace Drupal\stack_overflow\Entity;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Url;

/**
 * StackCommentViewBuilder.
 */
class StackCommentViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    parent::buildComponents($build, $entities, $displays, $view_mode);

    /** @var \Drupal\stack_overflow\Entity\StackCommentEntityInterface[] $entities */
    if (empty($entities)) {
      return;
    }

    foreach ($entities as $id => $entity) {
      $bundle = $entity->bundle();
      $display = $displays[$bundle];

      if ($display->getComponent('links')) {
        $build[$id]['links'] = [
          '#prefix' => '<ul class="links inline">',
          '#suffix' => '</ul>',
        ];

        $build[$id]['links']['edit'] = [
          '#type' => 'link',
          '#title' => $this->t('Edit'),
          '#attributes' => [
            'class' => 'use-ajax',
          ],
          '#url' => Url::fromRoute('stack_overflow.action.edit_content', [
            'entity_type' => $entity->getEntityTypeId(),
            'stack_overflow_entity' => $entity->id(),
          ]),
          '#access' => $entity->access('update'),
          '#prefix' => '<li class="so-comment-edit">',
          '#suffix' => '</li>',
        ];

        $build[$id]['links']['delete'] = [
          '#type' => 'link',
          '#title' => $this->t('Delete'),
          '#attributes' => [
            'class' => 'use-ajax',
          ],
          '#url' => Url::fromRoute('stack_overflow.action.delete_content', [
            'entity_type' => $entity->getEntityTypeId(),
            'stack_overflow_entity' => $entity->id(),
          ]),
          '#access' => $entity->access('delete'),
          '#prefix' => '<li class="so-comment-delete">',
          '#suffix' => '</li>',
        ];
      }
    }
  }

}
