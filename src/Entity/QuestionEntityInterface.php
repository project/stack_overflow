<?php

namespace Drupal\stack_overflow\Entity;

/**
 * Provides an interface for defining Question entities.
 *
 * @ingroup stack_overflow
 */
interface QuestionEntityInterface extends StackOverflowEntityInterface {

  /**
   * Return dependent answers from the current question.
   *
   * @return \Drupal\stack_overflow\Entity\AnswerEntityInterface[]
   *   Answer entities.
   */
  public function getAnswers();

}
