<?php

namespace Drupal\stack_overflow\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * StackOverflowEntityInterface.
 */
interface StackOverflowEntityInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the question creation timestamp.
   *
   * @return int
   *   Creation timestamp of the question.
   */
  public function getCreatedTime();

  /**
   * Sets the question creation timestamp.
   *
   * @param int $timestamp
   *   The question creation timestamp.
   *
   * @return \Drupal\stack_overflow\Entity\QuestionEntityInterface
   *   The called question entity.
   */
  public function setCreatedTime($timestamp);

}
