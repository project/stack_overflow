<?php

namespace Drupal\stack_overflow\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Stack comment entities.
 *
 * @ingroup stack_overflow
 */
interface StackCommentEntityInterface extends StackOverflowEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Sets a related entity to which the comment refers.
   *
   * @param \Drupal\stack_overflow\Entity\StackOverflowEntityInterface $entity
   *   The Question or Answer entity.
   *
   * @return \Drupal\stack_overflow\Entity\StackCommentEntityInterface
   *   The called Stack comment entity.
   */
  public function setRelatedEntity(StackOverflowEntityInterface $entity);

  /**
   * Get a parent entity, which was set by setRelatedEntity method.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The Question or Answer entity.
   */
  public function getRelatedEntity();

}
