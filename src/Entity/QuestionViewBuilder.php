<?php

namespace Drupal\stack_overflow\Entity;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\stack_overflow\StackOverflowManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * QuestionViewBuilder.
 */
class QuestionViewBuilder extends EntityViewBuilder {

  /**
   * StackOverflowManager.
   *
   * @var \Drupal\stack_overflow\StackOverflowManager
   */
  protected $stackOverflowManager;

  /**
   * QuestionViewBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   EntityTypeInterface.
   * @param \Drupal\stack_overflow\StackOverflowManager $stack_overflow_manager
   *   StackOverflowManager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   EntityRepositoryInterface.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   LanguageManagerInterface.
   * @param \Drupal\Core\Theme\Registry|null $theme_registry
   *   Registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface|null $entity_display_repository
   *   EntityDisplayRepositoryInterface.
   */
  public function __construct(EntityTypeInterface $entity_type, StackOverflowManager $stack_overflow_manager, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry = NULL, EntityDisplayRepositoryInterface $entity_display_repository = NULL) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);

    $this->stackOverflowManager = $stack_overflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('stack_overflow.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $build) {
    $build = parent::build($build);

    $question = $build['#question'];

    $build['comments'] = [
      '#type' => 'details',
      '#title' => $this->t('Comments: @count', [
        '@count' => $this->stackOverflowManager->getCountComments($question),
      ]),
      '#open' => FALSE,
      '#attributes' => [
        'id' => 'details--' . $question->getEntityTypeId() . '--' . $question->id(),
      ],
      '#weight' => 100,
    ];

    $build['answers'] = [
      '#prefix' => '<div id="answers"><h2>' . $this->t('Answers') . '</h2>',
      '#suffix' => '</div>',
      '#weight' => 110,
    ];

    $build['comments']['list'] = [
      '#prefix' => '<div class="comment-list">',
      '#suffix' => '</div>',
    ];

    $build['answers']['list'] = [
      '#prefix' => '<div id="answer-list">',
      '#suffix' => '</div>',
    ];

    $build['comments']['list']['content'] = $this->stackOverflowManager->getRenderComments($question);
    $build['comments']['form'] = $this->stackOverflowManager->getCommentForm($question);

    $build['answers']['list']['content'] = $this->stackOverflowManager->getRenderAnswers($question);
    $build['answers']['form'] = $this->stackOverflowManager->getAnswerForm($question);

    return $build;
  }

}
