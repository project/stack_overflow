<?php

namespace Drupal\stack_overflow;

use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\stack_overflow\Entity\QuestionEntityInterface;
use Drupal\stack_overflow\Entity\StackOverflowEntityInterface;

/**
 * StackOverflowManager.
 */
class StackOverflowManager {

  /**
   * EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EntityFormBuilderInterface.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * StackOverflowManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   EntityFormBuilderInterface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityTypeManagerInterface.
   */
  public function __construct(EntityFormBuilderInterface $entity_form_builder, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityFormBuilder = $entity_form_builder;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get add comment form for Question or Answer entity.
   *
   * @param \Drupal\stack_overflow\Entity\StackOverflowEntityInterface $related_entity
   *   StackOverflowEntityInterface.
   *
   * @return array
   *   Form render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCommentForm(StackOverflowEntityInterface $related_entity) {
    /** @var \Drupal\stack_overflow\Entity\StackCommentEntityInterface $entity */
    $entity = $this->entityTypeManager->getStorage('stack_comment')->create();
    $entity->setRelatedEntity($related_entity);

    return $this->entityFormBuilder->getForm($entity, 'edit_ajax', ['is_new' => TRUE]);
  }

  /**
   * Get list comment entities for Question or Answer entity.
   *
   * @param \Drupal\stack_overflow\Entity\StackOverflowEntityInterface $related_entity
   *   StackOverflowEntityInterface.
   *
   * @return array
   *   Build render array.
   */
  public function getRenderComments(StackOverflowEntityInterface $related_entity) {
    $comments = $related_entity->get('comments')->referencedEntities();
    $build = [];

    if ($comments && $comments[key($comments)]->access('view')) {
      $build_comments = $this->entityTypeManager
        ->getViewBuilder('stack_comment')
        ->viewMultiple($comments);

      $build['list'] = $build_comments;
    }

    return $build;
  }

  /**
   * Get count of comments by entity.
   *
   * @param \Drupal\stack_overflow\Entity\StackOverflowEntityInterface $related_entity
   *   StackOverflowEntityInterface.
   *
   * @return int
   *   Count.
   */
  public function getCountComments(StackOverflowEntityInterface $related_entity) {
    return $related_entity->get('comments')->count();
  }

  /**
   * Get list render Answers entities for Question.
   *
   * @param \Drupal\stack_overflow\Entity\QuestionEntityInterface $question
   *   QuestionEntityInterface.
   *
   * @return array
   *   Build render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRenderAnswers(QuestionEntityInterface $question) {
    $storage = $this->entityTypeManager->getStorage('answer');
    $build = [];

    $answers_ids = $storage->getQuery()
      ->condition('question', $question->id())
      ->execute();
    $answers = $storage->loadMultiple($answers_ids);

    if ($answers && $answers[key($answers)]->access('view')) {
      $view_builder = $this->entityTypeManager->getViewBuilder('answer');
      $build_answers = $view_builder->viewMultiple($answers);
      $build['list'] = $view_builder->buildMultiple($build_answers);
    }

    return $build;
  }

  /**
   * Add answer form for Question entity.
   *
   * @param \Drupal\stack_overflow\Entity\QuestionEntityInterface $question
   *   QuestionEntityInterface.
   *
   * @return array
   *   Form render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAnswerForm(QuestionEntityInterface $question) {
    $answer = $this->entityTypeManager->getStorage('answer')->create();
    $answer->set('question', $question->id());

    $form = $this->entityFormBuilder->getForm($answer, 'edit_ajax', ['is_new' => TRUE]);
    $form['#action'] = Url::fromRoute('entity.question.canonical', [
      'question' => $question->id(),
    ])->toString();

    return $form;
  }

  /**
   * Сhecks did the user has answered this question.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   AccountInterface.
   * @param \Drupal\stack_overflow\Entity\QuestionEntityInterface $question
   *   QuestionEntityInterface.
   *
   * @return bool
   *   Is user already add answer.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isUserAlreadyAddAnswer(AccountInterface $account, QuestionEntityInterface $question) {
    $answer_id = $this->entityTypeManager->getStorage('answer')
      ->getQuery()
      ->condition('uid', $account->id())
      ->condition('question', $question->id())
      ->execute();

    return ($answer_id);
  }

}
