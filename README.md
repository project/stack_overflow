Your own Stack Overflow
https://www.drupal.org/project/stack_overflow
=============================================

INTRODUCTION
------------

The module allows you create your own Stack Overflow site with questions,
answers and comments

REQUIREMENTS
------------

Coming soon...

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
-------------

Coming soon...

MAINTAINERS
-----------

Current maintainers:
* Miroslav Lee - https://www.drupal.org/u/miroslav-lee
* Andrei Ivnitskii - https://www.drupal.org/u/ivnish
